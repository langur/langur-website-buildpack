#!/usr/bin/env ruby

$:.unshift File.expand_path("../../lib", __FILE__)
require "language_pack"

if pack = LanguagePack::Octopress.new(ARGV[0], ARGV[1])
  pack.log("compile") do
    pack.compile
  end
end
